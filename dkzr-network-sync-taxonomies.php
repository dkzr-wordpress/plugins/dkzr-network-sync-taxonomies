<?php
/**
 * Plugin Name: Network Sync Taxonomies
 * Plugin URI: https://dkzr.nl
 * Update URI: https://api.dkzr.nl/wp/update-check/
 * Composer Package Name: dkzr/network-sync-taxonomies
 * Description: Sync taxonomies from the main site to sub-sites in a WordPress network install
 * Version: 2.1.2
 * Network: true
 * Author: Joost de Keijzer <j@dkzr.nl>, Marc de Bruijn <marc@puntpixel.nl>
 * Text Domain: dkzr-nst
 * Domain Path: languages
 * License: GPL2
 */

/**
 * WPML: look into WPML_Sync_Term_Meta_Action::sync_custom_field() to copy term meta into sub-sites
 */

namespace DKZR;

class NetworkSyncTaxonomies {
  protected $origin = 0;
  protected $taxonomies = [];
  protected $sites = [];

 // TODO: add settings pages?
  public function __construct() {
    // `wp_loaded` runs after `init`
    add_action( 'wp_loaded', [ $this, 'wp_loaded' ] );
  }

/**
 * Init Network Sync taxonomies
 *
 * For now: use hooks to configure the plugin.
 */
  public function wp_loaded() {
    $this->origin     = (int) apply_filters( 'dkzr-nst-origin', get_main_site_id() );

    if ( is_multisite() && $this->origin ) {
      $this->taxonomies = array_filter( apply_filters( 'dkzr-nst-taxonomies', $this->taxonomies ) );
      $this->sites      = array_filter( apply_filters( 'dkzr-nst-sites', get_sites( [ 'fields' => 'ids' ] ) ) );

      $this->hooks( 'add' );

      add_action( 'wp_initialize_site', [ $this, 'wp_initialize_site' ], 999999, 1 );
      add_action( 'admin_action_nst_resync', [ $this, 'admin_action_nst_resync' ] );
    }

    add_filter( 'plugin_row_meta', [ $this, 'plugin_row_meta' ], 10, 4 );
  }

  public function plugin_row_meta( $plugin_meta, $plugin_file, $plugin_data, $status ) {
    if (
      trailingslashit( WP_PLUGIN_DIR ) . $plugin_file == __FILE__
      && is_multisite()
      && is_admin()
      && current_user_can( 'manage_sites' )
      && get_current_screen()
      && 'plugins-network' == get_current_screen()->id
    ) {
      $plugin_meta[] = sprintf( '<a href="%s">%s</a>', esc_attr( add_query_arg( [ 'action' => 'nst_resync', ], get_admin_url() ) ), 'Resync taxonomies' );
    }
    return $plugin_meta;
  }

  protected function hooks( $action = 'add' ) {
    if ( 'add' == $action ) {
      $action_function = 'add_action';
      $filter_function = 'add_filter';
    } else {
      $action_function = 'remove_action';
      $filter_function = 'remove_filter';
    }

    $action_function( 'create_term',        [ $this, 'saved_term' ],      1, 3 );
    $action_function( 'edited_term',        [ $this, 'saved_term' ],      1, 3 );
    $action_function( 'pre_delete_term',    [ $this, 'pre_delete_term' ], 1, 2 );

    $filter_function( 'add_term_metadata',    [ $this, 'add_term_metadata' ], 99999, 5 );
    $filter_function( 'update_term_metadata',  [ $this, 'update_term_metadata' ], 99999, 5 );

    $action_function( 'delete_term_meta',  [ $this, 'delete_term_meta' ], 10, 4 );
  }

  public function saved_term( $term_id, $tt_id, $taxonomy ) {
    global $wpdb, $sitepress;

    if ( ! in_array( $taxonomy, $this->taxonomies ) ) {
      return;
    }

    $post_backup = $_POST;
    $this->hooks( 'remove' );

    $blog_id = get_current_blog_id();
    $origin = sprintf( '%d.%d', $this->origin, $term_id );

    if ( isset( $sitepress ) ) {
      add_filter( 'pre_wpml_is_translated_taxonomy', '__return_false' );
    }

    $term = get_term( $term_id, $taxonomy );
    $term = wp_slash( (array) $term->data );

    $parent = null;
    if ( $term[ 'parent' ] ) {
      $parent = get_term( $term[ 'parent' ], $taxonomy );
      $parent = wp_slash( (array) $parent->data );
    }

    if ( $blog_id != $this->origin ) {
      // We're _not_ in origin!
      $_term_id = $this->get_origin( $term_id )[ 'term_id' ];
      $add_origin = ( $_term_id ? false : true );

      $_parent_term_id = 0;
      if ( $parent ) {
        $_parent_term_id = $this->get_origin( $parent[ 'term_id' ] )[ 'term_id' ];
      }

      if ( isset( $sitepress ) ) {
        $translation = $wpdb->get_results( $wpdb->prepare( "SELECT t2.element_id, t2.element_type, t2.trid, t2.language_code, t2.source_language_code FROM {$wpdb->prefix}icl_translations t1 INNER JOIN {$wpdb->prefix}icl_translations t2 ON ( t1.trid = t2.trid ) WHERE t1.element_type = %s AND t1.element_id = %d", 'tax_' . $taxonomy, $term_id ), OBJECT_K );

        foreach( array_keys( $translation ) as $element_id ) {
          $translation[ $element_id ]->nst_origin = $this->get_origin( $element_id, true );
        }
      }

      if ( isset( $sitepress ) ) {
        remove_filter( 'pre_wpml_is_translated_taxonomy', '__return_false' );
      }

      switch_to_blog( $this->origin );

      if ( isset( $sitepress ) ) {
        add_filter( 'pre_wpml_is_translated_taxonomy', '__return_false' );
      }

      $args = $term;
      if ( $parent ) {
        $_parent = get_term( $_parent_term_id, $taxonomy );
        if ( $_parent ) {
          $args[ 'parent' ] = $_parent->term_id;
        } else {
          // TODO: fix this later?
          $args[ 'parent' ] = 0;
        }
      }

      // check if _term_id actually exists
      if ( ! get_term_by( 'term_id', $_term_id, $taxonomy ) ) {
        $_term_id = 0;
        $add_origin = true;
      }

      if ( $_term_id ) {
        // term exists, update
        $_term_ids = wp_update_term( $_term_id, $taxonomy, $args );
      } else {
        // term does not exist, insert
        $_term_ids = wp_insert_term( $term[ 'name' ], $taxonomy, $args );
      }

      if ( is_wp_error( $_term_ids ) ) {
        if ( $_term_id ) {
          $_term_ids = [ 'term_id' => $_term_id ];
        } else {
          // duplicate slug most likely error
          $_term_ids = get_term_by( 'slug', $args['slug'], $taxonomy, ARRAY_A);
        }
      }

      if ( isset( $sitepress ) ) {
        remove_filter( 'pre_wpml_is_translated_taxonomy', '__return_false' );

        // first: sync non-origin to origin
        if ( isset( $translation ) ) {
          $this->sync_wpml_translations( $taxonomy, $translation, [ $term_id => $_term_ids['term_id'] ] );
        }

        // re-create $translation for syncing origin to non-origin
        $translation = $wpdb->get_results( $wpdb->prepare( "SELECT t2.element_id, t2.element_type, t2.trid, t2.language_code, t2.source_language_code FROM {$wpdb->prefix}icl_translations t1 INNER JOIN {$wpdb->prefix}icl_translations t2 ON ( t1.trid = t2.trid ) WHERE t1.element_type = %s AND t1.element_id = %d", 'tax_' . $taxonomy, $_term_ids['term_id'] ), OBJECT_K );
      }

      restore_current_blog();

      $origin = sprintf( '%d.%d', $this->origin, $_term_ids[ 'term_id' ] );

      if ( $add_origin ) {
        add_term_meta( $term_id, 'dkzr-nst-origin', $origin, true );
      }
    } else {
      if ( isset( $sitepress ) ) {
        $translation = $wpdb->get_results( $wpdb->prepare( "SELECT t2.element_id, t2.element_type, t2.trid, t2.language_code, t2.source_language_code FROM {$wpdb->prefix}icl_translations t1 INNER JOIN {$wpdb->prefix}icl_translations t2 ON ( t1.trid = t2.trid ) WHERE t1.element_type = %s AND t1.element_id = %d", 'tax_' . $taxonomy, $term_id ), OBJECT_K );
      }
    }

    if ( isset( $sitepress ) ) {
      remove_filter( 'pre_wpml_is_translated_taxonomy', '__return_false' );
    }

    foreach( $this->sites as $site ) {
      if ( $this->origin == $site ) {
        // don't edit origin site
        continue;
      }

      if ( $blog_id == $site ) {
        // don't edit current site, just sync WPML
        if ( isset( $sitepress, $translation ) ) {
          $this->sync_wpml_translations( $taxonomy, $translation );
        }

        continue;
      }

      switch_to_blog( $site );

      $args = $term;
      if ( $parent ) {
        $_parent = get_term_by( 'slug', $parent[ 'slug' ], $taxonomy );
        if ( $_parent ) {
          $args[ 'parent' ] = $_parent->term_id;
        } else {
          // TODO: fix this later?
          $args[ 'parent' ] = 0;
        }
      }

      $_term_id = $this->get_term_from_origin( $origin );
      if ( $_term_id ) {
        // term exists, update
        $_term_ids = wp_update_term( $_term_id, $taxonomy, $args );
      } else {
        // term does not exist, insert
        $_term_ids = wp_insert_term( $term[ 'name' ], $taxonomy, $args );
        if ( is_wp_error( $_term_ids ) ) {
          $_term_ids = get_term_by( 'slug', $term[ 'slug' ], $taxonomy, ARRAY_A );
        }
        add_term_meta( $_term_ids[ 'term_id' ], 'dkzr-nst-origin', $origin, true );
      }

      if ( isset( $sitepress, $translation ) ) {
        $this->sync_wpml_translations( $taxonomy, $translation );
      }

      restore_current_blog();
    }

    $_POST = $post_backup;

    $this->hooks( 'add' );
  }

  /**
   * Runs before actual deletion
   */
  public function pre_delete_term( $term, $taxonomy ) {
    global $wpdb;

    if ( ! in_array( $taxonomy, $this->taxonomies ) ) {
      return;
    }

    $post_backup = $_POST;
    $this->hooks( 'remove' );

    $blog_id = get_current_blog_id();
    $origin = sprintf( '%d.%d', $this->origin, $term );

    if ( $blog_id != $this->origin ) {
      // We're _not_ in origin!
      $_term_id = $this->get_origin( $term )[ 'term_id' ];

      switch_to_blog( $this->origin );

      wp_delete_term( $_term_id, $taxonomy );

      restore_current_blog();

      $origin = sprintf( '%d.%d', $this->origin, $_term_id );
    }

    foreach( $this->sites as $site ) {
      if ( $blog_id == $site || $this->origin == $site ) {
        // don't edit current site AND don't edit origin site
        continue;
      }

      switch_to_blog( $site );

      $_term_id = $this->get_term_from_origin( $origin );
      if ( $_term_id ) {
        wp_delete_term( $_term_id, $taxonomy );
      }

      restore_current_blog();
    }

    $_POST = $post_backup;
    $this->hooks( 'add' );
  }

  /**
   * In default WordPress environment, runs _before_ the change has been done in the active site.
   * This method _may_not_ change the `$return` argument.
   */
  public function add_term_metadata( $return, $object_id, $meta_key, $meta_value, $unique ) {
    $term = get_term( $object_id );

    if ( $term && in_array( $term->taxonomy, $this->taxonomies ) ) {
      $post_backup = $_POST;
      $this->hooks( 'remove' );

      $blog_id = get_current_blog_id();
      $origin = $this->get_origin( $object_id, false );

      foreach( $this->sites as $site ) {
        if ( $blog_id == $site ) {
          // don't edit current site
          continue;
        }

        switch_to_blog( $site );

        $term_id = $this->get_term_from_origin( $origin );
        add_term_meta( $term_id, $meta_key, $meta_value, $unique );

        restore_current_blog();
      }

      $_POST = $post_backup;
      $this->hooks( 'add' );
    }

    return $return;
  }


  /**
   * In default WordPress environment, runs _before_ the change has been done in the active site.
   * This method _may_not_ change the `$return` argument.
   */
  public function update_term_metadata( $return, $object_id, $meta_key, $meta_value, $prev_value ) {
    global $wpdb;
    $term = get_term( $object_id );

    if ( $term && in_array( $term->taxonomy, $this->taxonomies ) ) {
      $post_backup = $_POST;
      $this->hooks( 'remove' );

      $blog_id = get_current_blog_id();
      $origin = $this->get_origin( $object_id, false );

      foreach( $this->sites as $site ) {
        if ( $blog_id == $site ) {
          // don't edit current site
          continue;
        }

        switch_to_blog( $site );

        $term_id = $this->get_term_from_origin( $origin );
        $mid = update_term_meta( $term_id, $meta_key, $meta_value, $prev_value );
        if ( is_int( $mid ) && 0 < $mid ) {
          // `update_term_meta` could not find current value so it created a new term_meta row.
          // Since `update_term_meta` in `$blog_id` (the blog the user is editing in) will call
          // `add_term_meta` lateron we'll get two entries.
          // No chance to fix it later, so just delete the newly created entry here...
          $wpdb->delete( $wpdb->termmeta, [ 'meta_id' => $mid ] );
          wp_cache_delete( $term_id, 'term_meta' );
        }

        restore_current_blog();
      }

      $_POST = $post_backup;
      $this->hooks( 'add' );
    }

    return $return;
  }

  /**
   * Runs before actual deletion
   */
  public function delete_term_meta( $meta_ids, $object_id, $meta_key, $_meta_value ) {
    $term = get_term( $object_id );

    if ( $term && in_array( $term->taxonomy, $this->taxonomies ) ) {
      $blog_id = get_current_blog_id();
      $origin = $this->get_origin( $object_id, false );

      // Short-cirquit: when the term is deleted, all termmeta is deleted
      // automaticly which may result in the origin not being available anymore.
      if ( empty( $origin ) ) {
        return;
      }

      $post_backup = $_POST;
      $this->hooks( 'remove' );

      foreach( $this->sites as $site ) {
        if ( $blog_id == $site ) {
          // don't edit current site
          continue;
        }

        switch_to_blog( $site );

        $term_id = $this->get_term_from_origin( $origin );
        delete_term_meta( $term_id, $meta_key, $_meta_value );

        restore_current_blog();
      }

      $_POST = $post_backup;
      $this->hooks( 'add' );
    }
  }

  public function admin_action_nst_resync() {
    if ( is_admin() && current_user_can( 'manage_sites' ) ) {
      if ( isset( $_GET['nts_resync_done'] ) ) {
        wp_die( sprintf( 'Resyncing Network Sync Taxonomnies is comleted<br><a href="%s">back to wp-admin</a> or <a href="%s">resync again</a>', esc_attr( get_admin_url() ), esc_attr( add_query_arg( [
            'action' => 'nst_resync',
            'nts_resync_sites' => [0],
          ], get_admin_url() ) ) ), 'Network Sync Taxonomnies Resync' );
        exit;
      }

      if ( ! array_key_exists( 'nts_resync_sites', $_GET ) ) {
        wp_die( sprintf('Are you sure you want to resync all Network Sync Taxonomnies?<br><a href="%s">back to wp-admin</a> or <a href="%s">start resync</a>', esc_attr( get_admin_url() ), esc_attr( add_query_arg( [
            'action' => 'nst_resync',
            'nts_resync_sites' => [0],
          ], get_admin_url() ) ), 'Network Sync Taxonomnies Resync' ) );
        exit;
      }

      $sites = $_GET['nts_resync_sites'] ?? [];
      foreach ( $this->sites as $site_id ) {
        if ( ! in_array( $site_id, $sites ) ) {
          $sites[] = $site_id;

          $this->wp_initialize_site( get_site( $site_id ) );

          wp_redirect( add_query_arg( [
            'action' => 'nst_resync',
            'nts_resync_sites' => $sites,
          ], get_admin_url() ) );
          exit;
        }
      }

      wp_redirect( add_query_arg( [
        'action' => 'nst_resync',
        'nts_resync_done' => 1,
      ], get_admin_url() ) );
      exit;
    } else {
      wp_die( 'Resyncing Network Sync Taxonomnies is only allowed for Super Admin users', 'Action not allowed' );
      exit;
    }
  }

  public function wp_initialize_site( \WP_Site $new_site ) {
    global $wpdb, $sitepress;

    if ( $new_site->blog_id == $this->origin ) {
      // Should not be possible, but handle anyway
      return;
    }

    $this->hooks( 'remove' );

    $taxonomies   = [];
    $translations = [];

    switch_to_blog( $this->origin );

    if ( isset( $sitepress ) ) {
      add_filter( 'pre_wpml_is_translated_taxonomy', '__return_false' );
    }

    // get
    foreach( $this->taxonomies as $taxonomy ) {
      $taxonomies[ $taxonomy ] = [];
      foreach( get_terms( [
        'taxonomy'          => $taxonomy,
        'orderby'           => 'term_id',
        'hide_empty'        => false,
        'number'            => 0, // all
        'fields'            => 'all',
        'suppress_filter'   => true,
        'wpml_skip_filters' => true,
      ] ) as $term ) {
        $taxonomies[ $taxonomy ][ $term->term_id ] = $term;
      }

      $translations[ $taxonomy ] = [];
      if ( isset( $sitepress ) ) {
        foreach( $wpdb->get_results( $wpdb->prepare( "SELECT element_type, element_id, trid, language_code, source_language_code FROM {$wpdb->prefix}icl_translations WHERE element_type = %s", 'tax_' . $taxonomy ) ) as $translation ) {
          if ( ! isset( $translations[ $taxonomy ][ $translation->trid ] ) ) {
            $translations[ $taxonomy ][ $translation->trid ] = [];
          }

          $translations[ $taxonomy ][ $translation->trid ][ $translation->element_id ] = $translation;

          if ( isset( $taxonomies[ $taxonomy ][ $translation->element_id ] ) ) {
            $taxonomies[ $taxonomy ][ $translation->element_id ]->icl_trid          = $translation->trid;
            $taxonomies[ $taxonomy ][ $translation->element_id ]->icl_language_code = $translation->language_code;
          }
        }
      }
    }

    if ( isset( $sitepress ) ) {
      remove_filter( 'pre_wpml_is_translated_taxonomy', '__return_false' );
    }

    restore_current_blog();

    switch_to_blog( $new_site->blog_id );

    if ( function_exists( 'icl_sitepress_activate' ) && false === get_option( 'icl_sitepress_version', false ) ) {
      icl_sitepress_activate();
    }

    if ( isset( $sitepress ) ) {
      add_filter( 'pre_wpml_is_translated_taxonomy', '__return_false' );
    }

    $inserted = [];

    // insert
    foreach( $taxonomies as $taxonomy => $terms ) {
      foreach( $terms as $term_id => $term ) {
        $inserted = $this->wp_initialize_site_insert_term( $taxonomy, $term_id, $term, $taxonomies, $inserted );
      }
    }

    if ( isset( $sitepress ) ) {
      remove_filter( 'pre_wpml_is_translated_taxonomy', '__return_false' );

      foreach( array_keys( $inserted ) as $origin_id ) {
        $inserted = $this->wp_initialize_site_insert_translation( $origin_id, $taxonomies, $translations, $inserted );
      }
    }

    restore_current_blog();

    $this->hooks( 'add' );
  }

  protected function wp_initialize_site_insert_term( $taxonomy, $term_id, $term, $taxonomies, $inserted ) {
    if ( get_current_blog_id() == $this->origin ) {
      return $inserted;
    }

    if ( isset( $inserted[ $term_id ] ) ) {
      return $inserted;
    }

    if ( $term->parent && ! isset( $inserted[ $term->parent ] ) ) {
      $_term_id = $term->parent;
      if ( isset( $taxonomies[ $taxonomy ][ $_term_id ] ) ) {
        $_term = $taxonomies[ $taxonomy ][ $_term_id ];
        $inserted = $this->wp_initialize_site_insert_term( $taxonomy, $_term_id, $_term, $taxonomies, $inserted );
      } else {
        // TODO: fix this later?
        $term->parent = 0;
      }
    }

    $args = wp_slash( (array) $term->data );
    if ( $term->parent ) {
      $args[ 'parent' ] = $inserted[ $term->parent ]['term_id'];
    }

    $_term_ids = wp_insert_term( $term->name, $taxonomy, $args );
    if ( is_wp_error( $_term_ids ) ) {
      $_term_ids = get_term_by( 'slug', $term->slug, $taxonomy, ARRAY_A );
    }

    $inserted[ $term->term_id ] = [
      'taxonomy'  => $taxonomy,
      'term_id'   => $_term_ids[ 'term_id' ],
    ];
    add_term_meta( $_term_ids[ 'term_id' ], 'dkzr-nst-origin', sprintf( '%d.%d', $this->origin, $term->term_id ), true );

    return $inserted;
  }

  protected function wp_initialize_site_insert_translation( $origin_id, $taxonomies, $translations, $inserted ) {
    global $wpdb, $sitepress;

    if ( ! isset( $sitepress ) ) {
      return $inserted;
    }

    if ( isset( $inserted[ $origin_id ]['translated'] ) ) {
      return $inserted;
    }

    // Extract $taxonomy and $term_id
    extract( $inserted[ $origin_id ] );

    $_trid          = $taxonomies[ $taxonomy ][ $origin_id ]->icl_trid ?? false;

    if ( $_trid ) {
      $translation = $translations[ $taxonomy ][ $_trid ] ?? false;
    }

    if ( empty( $translation ) ) {
      return $inserted;
    }

    $this->sync_wpml_translations( $taxonomy, $translation );

    foreach( $translation as $lang ) {
      $inserted[ $lang->element_id ]['translated'] = true;
    }
    $inserted[ $origin_id ]['translated'] = true;

    return $inserted;
  }

  protected function sync_wpml_translations( $taxonomy, $translation, $force_element_ids = [] ) {
    global $wpdb;

    $element_ids = [];
    $source_id = 0;
    foreach(  $translation as $lang ) {
      if ( isset( $lang->nst_origin['term_id'] ) ) {
        $element_ids[ $lang->element_id ] = (int) $wpdb->get_var( $wpdb->prepare( "SELECT term_id FROM $wpdb->terms WHERE `term_id` = %d", $lang->nst_origin['term_id'] ) );
      } else {
        $element_ids[ $lang->element_id ] = $this->get_term_from_origin( sprintf( '%d.%d', $this->origin, $lang->element_id ) );
      }

      if ( empty( $lang->source_language_code ) ) {
        $source_id = $lang->element_id;
      }
    }

    foreach( array_filter( $force_element_ids ) as $key => $value ) {
      $element_ids[ $key ] = $value;
    }

    $element_ids = array_filter( $element_ids );

    if ( empty( $source_id ) || empty( $element_ids[ $source_id ] ) ) {
      $source_id = reset( $element_ids );
    }

    $trid = (int) $wpdb->get_var( $wpdb->prepare( "SELECT trid FROM {$wpdb->prefix}icl_translations WHERE element_type = %s AND element_id = %d", 'tax_' . $taxonomy, $this->get_term_from_origin( sprintf( '%d.%d', $this->origin, $source_id ) ) ) );

    // keys are language_code (trid-language_code must be unique index)
    $trid_lang = [];

    if ( empty( $trid ) ) {
      $trid = 1 + (int) $wpdb->get_var( "SELECT MAX(trid) FROM {$wpdb->prefix}icl_translations" );
    } else {
      $trid_lang = $wpdb->get_results( $wpdb->prepare( "SELECT language_code, translation_id, element_type, element_id, source_language_code FROM {$wpdb->prefix}icl_translations WHERE trid = %d", $trid ), OBJECT_K );
    }

    foreach( $element_ids as $origin_id => $element_id ) {
      if (
        empty( $translation[ $origin_id ] )
        || ! is_object( $translation[ $origin_id ] )
        || ! property_exists( $translation[ $origin_id ], 'language_code' )
        || ! property_exists( $translation[ $origin_id ], 'source_language_code' )
      ) {
        continue;
      }

      if ( isset( $trid_lang[ $translation[ $origin_id ]->language_code ] ) ) {
        if (
          $trid_lang[ $translation[ $origin_id ]->language_code ]->element_id === $element_id
          && $trid_lang[ $translation[ $origin_id ]->language_code ]->source_language_code === $translation[ $origin_id ]->source_language_code
        ) {
          /**
           * All relevant is correctly set:
           * - element_type (from initial trid search)
           * - language_code (from trid_lang search)
           * - element_id (from this test)
           * - source_language_code (from this test)
           *
           * So continue to next element_id
           */
          continue;
        } else {
          $translation_id = $trid_lang[ $translation[ $origin_id ]->language_code ]->translation_id;
        }
      } else {
        $translation_id = (int) $wpdb->get_var( $wpdb->prepare( "SELECT translation_id FROM {$wpdb->prefix}icl_translations WHERE element_type = %s AND element_id = %d AND language_code = %s", 'tax_' . $taxonomy, $element_id, $translation[ $origin_id ]->language_code ) );
      }

      if ( $translation_id ) {
        $wpdb->update(
          "{$wpdb->prefix}icl_translations",
          [
            'trid'                  => $trid,
            'language_code'         => $translation[ $origin_id ]->language_code,
            'source_language_code'  => $translation[ $origin_id ]->source_language_code,
          ],
          [ 'translation_id' => $translation_id ]
        );
      } else {
        // remove translations with differend trid, if available
        $wpdb->query( $wpdb->prepare( "DELETE FROM {$wpdb->prefix}icl_translations WHERE element_type = %s AND element_id = %d AND trid <> %d", "tax_{$taxonomy}", $element_id, $trid ) );

        $wpdb->insert(
          "{$wpdb->prefix}icl_translations",
          [
            'element_type'          => 'tax_' . $taxonomy,
            'element_id'            => $element_id,
            'trid'                  => $trid,
            'language_code'         => $translation[ $origin_id ]->language_code,
            'source_language_code'  => $translation[ $origin_id ]->source_language_code,
          ]
        );
      }
    }
  }

  protected function get_origin( $term, $explode = true ) {
    if ( get_current_blog_id() == $this->origin ) {
      $origin = sprintf( '%d.%d', $this->origin, (int) $term );
    } else {
      $origin = get_term_meta( (int) $term, 'dkzr-nst-origin', true );
    }

    if ( $explode ) {
      $blog_id = $this->origin;
      $term_id = 0;
      if ( $origin ) {
        list( $blog_id, $term_id ) = explode( '.', $origin );
      }
      return compact( 'blog_id', 'term_id' );
    } else {
      return $origin;
    }
  }

  protected function get_term_from_origin( $origin ) {
    global $wpdb;

    if ( empty( $origin ) || strpos( $origin, '.' ) === false ) {
      return 0;
    }

    list( $blog_id, $term_id ) = explode( '.', $origin );

    if ( get_current_blog_id() == $this->origin ) {
      return $term_id;
    } else {
      return $wpdb->get_var( $wpdb->prepare( "SELECT term_id FROM $wpdb->termmeta WHERE `meta_key` = 'dkzr-nst-origin' AND `meta_value` = %s", $origin ) );
    }
  }
}

global $dkzrNetworkSyncTaxonomies;
$dkzrNetworkSyncTaxonomies = new NetworkSyncTaxonomies();

//if ( WP_DEBUG ) {
//  add_filter( 'dkzr-nst-taxonomies', function( $taxonomies ) {
//    return $taxonomies + [ 'category' ];
//  } );
//
//  add_action( 'wp_loaded', function() {
//    if ( isset( $_REQUEST['dkzr-nst'] ) ) {
//      if ( 'add' == $_REQUEST['dkzr-nst'] ) {
//        $term = wp_insert_term( 'test 123', 'category' );
//      }
//      if ( 'delete' == $_REQUEST['dkzr-nst'] && isset( $_REQUEST['dkzr-nst-id'] ) ) {
//        wp_delete_term( $_REQUEST['dkzr-nst-id'], 'category' );
//      }
//      if ( 'update-meta' == $_REQUEST['dkzr-nst'] && isset( $_REQUEST['dkzr-nst-id'] ) ) {
//        update_term_meta( $_REQUEST['dkzr-nst-id'], 'dkzr-nst-test-meta', '1234' );
//      }
//      if ( 'update-meta2' == $_REQUEST['dkzr-nst'] && isset( $_REQUEST['dkzr-nst-id'] ) ) {
//        update_term_meta( $_REQUEST['dkzr-nst-id'], 'dkzr-nst-test-meta', '1235', '123' );
//      }
//      if ( 'add-meta' == $_REQUEST['dkzr-nst'] && isset( $_REQUEST['dkzr-nst-id'] ) ) {
//        add_term_meta( $_REQUEST['dkzr-nst-id'], 'dkzr-nst-test-meta', '123' );
//      }
//      if ( 'delete-meta' == $_REQUEST['dkzr-nst'] && isset( $_REQUEST['dkzr-nst-id'] ) ) {
//        delete_term_meta( $_REQUEST['dkzr-nst-id'], 'dkzr-nst-test-meta' );
//      }
//    }
//  }, 100 );
//}
