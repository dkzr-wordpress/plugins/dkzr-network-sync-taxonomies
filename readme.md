# Network Sync Taxonomies

Stable tag: 1.0
Requires at least: 5.9
Tested up to: 5.9  
Requires PHP: 7.0
License: GPL v2 or later
Contributors: joostdekeijzer

## Description

Sync taxonomies from the main site to sub-sites in a WordPress network install

### Usage

1. Activate this *Network* Plugin.
2. Register your taxonomies in a (custom) network enabled plugin. The taxonomies you want to sync *must* be present in all sites (blogs).
3. Configure the plugin using filter `hooks` on the WordPress `init` action in a network enabled plugin: all sites (blogs) *must* know of the sync settings.

### Hooks

The configuration filter hooks are run on the `wp_loaded` action. Please set your configurations on the `init` action which is run just before it.

The `dkzr-nst-taxonomies` filter manages the taxonomies you want to sync.

The `dkzr-nst-origin` filter defines the 'origin' of all taxonomies, defaults to the default blog id set with the `BLOG_ID_CURRENT_SITE` constant in `wp-config.php`.

The `dkzr-nst-sites` filter manages the sites (blogs) the taxonomies are synced to. Defaults to all.

## Frequently Asked Questions

## Changelog

### 1.0.1

- Configuration filter hooks are run on `wp_loaded` because most themes and  plugins run on `init` before it.

### 1.0

- Initial release.
